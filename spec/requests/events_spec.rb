require 'rails_helper'

RSpec.describe "Events", type: :request do
  include Devise::Test::IntegrationHelpers

  before do
    #necessary for devise login also above mentioned include
    @user = User.create(email: 'test@example.com', password: 'password', password_confirmation: 'password')
    sign_in @user
  end

  describe '#event_process_action' do
    it 'sends a campaign email' do
      allow(IterableService).to receive(:send_user_mail)
      post '/event_process_action', params: { event_action: 'Send user a campaign email', email: 'example@example.com' }
      expect(response).to redirect_to(root_path)
    end

    it 'gets user events' do
      allow(IterableService).to receive(:get_user_events)
      post '/event_process_action', params: { event_action: 'Get user events', email: 'example@example.com' }
      expect(response).to redirect_to(root_path)
    end

    it 'renders unknown action' do
      post '/event_process_action', params: { event_action: 'Invalid action' }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
