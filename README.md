<p>
  <img align="left" src="https://coffeebeans.io/images/home/cbLogoNew.svg" height="50px" >
</p>
<br />

---
<br />

## Event Trigger - Ruby/Rails Web Application

This web application explore the authentication process in rails 7 and iterable campign.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and require basic knowledge of,

```
* Ruby '~> 3.1.2'
* Rails '~> '7.0.3'
* tailwindcss-rails, '~> 2.3'
```

### Installing

A step by step series of examples that tell you how to get a development env running

```
$ git clone git@gitlab.com:anshulriyal/eventapi.git
$ cd eventapi/
```

#### Running

```
$ bundle install
$ rails db:prepare
$ bundle exec rails s
```
Now you can visit http://localhost:3000

![Output](https://gitlab.com/anshulriyal/eventapi/raw/main/public/output.png "Optional Title")

You're all set to play with the Application 🎮

## Built With

* [Ruby on Rails](https://rubyonrails.org/) - The web framework used
* [Ruby Version Manager (RVM)](https://rvm.io/) - Ruby Version Manager
