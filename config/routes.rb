Rails.application.routes.draw do
  get 'events/index'
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  resources :events, only: [:index, :create]
  root 'events#index'
  post '/event_process_action', to: 'events#event_process_action', as: 'event_process_action'
end
