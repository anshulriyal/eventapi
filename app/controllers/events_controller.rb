class EventsController < ApplicationController
  before_action :authenticate_user!
  protect_from_forgery except: [:send_user_mail]
  def index
  end

  def event_process_action
    case params[:event_action]
    when 'Send user a campaign email'
      email = params[:email]
      IterableService.send_user_mail(email)
      redirect_to root_path, notice: "Campaign email sent to #{email}"
      puts "--------x-----------------"
    when 'Get user events'
      email = params[:email]
      IterableService.get_user_events(email)
      redirect_to root_path, notice: "User events retrieved for #{email}"
    else
      render plain: 'Unknown action', status: :unprocessable_entity
    end
  end
end
