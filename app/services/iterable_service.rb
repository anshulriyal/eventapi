class IterableService
  include HTTParty
  base_uri 'https://api.iterable.com/api'
  class IntegrationError < StandardError; end

  def self.send_user_mail(email)
    puts "Sending campaign email to #{email}"
    #using endpoint from here https://api.iterable.com/api/docs#email_target
    target_email_request = {
      campaignId: 0,
      recipientEmail: email,
      dataFields: {},
      sendAt: Time.now.utc.strftime('%Y-%m-%d %H:%M:%S'),
      allowRepeatMarketingSends: true,
      metadata: {}
    }
    response = post('/email/target', body: target_email_request.to_json, headers: headers)
    handle_response(response)
  end

  def self.get_user_events(email)
    puts "Getting user events for #{email}"
    response = get("/events/#{email}", headers: headers)
    handle_response(response)
  end

  private

  def self.headers
    {
      'Content-Type' => 'application/json',
      'Api-Key' => '123_API_KEY_321' # Need to create account for same.
    }
  end

  def self.handle_response(response)
    if response.success?
      puts "API request successful: #{response.body}"
    else
      puts "API request failed: #{response.code}, #{response.body}"
      raise IntegrationError.new("API request failed: #{response.code}, #{response.body}", response.code)
    end
  end
end
